function socks() {
  kill -9 $(ss -p -t -l src 127.0.0.1:6128 | grep -oP 'pid=\K[0-9]*') 2>/dev/null
  ssh -f -D 6128 -N socks@ceres
}

function kssh() {
  ps auxf | grep ssh | grep -ve 'socks\|grep' | awk '{print $2}' | xargs -i kill -9 '{}'
}
