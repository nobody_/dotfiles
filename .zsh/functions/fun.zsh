function okay() {
  case "$1" in
    "mcabber")
      rm -f "$HOME"/.mcabber/.incoming
      ;;
    "update")
      yaourt -Syu
      ;;
    "dafuck")
      man "$2"
      ;;
    "bye")
      /usr/bin/su -l -c 'shutdown -h'
      ;;
    "train")
      /usr/local/games/sl
      ;;
    "work")
      /usr/bin/htop
      ;;
    "bird")
      /usr/bin/cat "$HOME/.trash/ascii/bird"
      ;;
    "cow")
      /usr/bin/cat "$HOME/.trash/ascii/cow"
      ;;
    "woman")
      /usr/bin/cat "$HOME/.trash/ascii/woman"
      ;;
    "meow")
      /usr/bin/cat "$HOME/.trash/ascii/meow"
      ;;
    "time")
      myat
      ;;
    *)
      echo "what?"
      ;;
  esac
}
compdef _okay okay
