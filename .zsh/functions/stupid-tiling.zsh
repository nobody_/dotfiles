function window-split() {
  local window=${1:-Firefox}
  local position=${2:-left}
  local resolution=$(xrandr  | grep \* | cut -d' ' -f4)
  local x=$(echo "$resolution" | cut -f1 -d'x')
  local y=$(echo "$resolution" | cut -f2 -d'x')
  local bar_width=52

  if [ "$position" = "left" ]; then
    position=0
  else
    position=$(( $x/2 ))
  fi


  # make window halt-screen size
  wmctrl -r "$window" -e 1,$position,0,$(( $x/2 )),$(( $y - $bar_width ))
  # add always on top
  wmctrl -r "$window" -b add,above
}

function _window-flush-flags() {
    wmctrl -i -r "$window" -b add,below
    wmctrl -i -r "$window" -b remove,below
}

function window-regular() {
  local resolution=$(xrandr  | grep \* | cut -d' ' -f4)
  local x=$(echo "$resolution" | cut -f1 -d'x')
  local y=$(echo "$resolution" | cut -f2 -d'x')
  local bar_width=52

  for window in $(wmctrl -l |  grep -viE "skype|$(hostname)\" $" | cut -f1 -d' '); do
    wmctrl -i -r "$window" -e 1,0,0,$x,$(( $y - $bar_width ))
    _window-flush-flags "$window"
  done
  _window-flush-flags skype
  wmctrl -r skype -e 1,500,100,1100,700
}
