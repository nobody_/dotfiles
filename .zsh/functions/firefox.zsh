function patch-ff() {
  pkill -9 firefox

  pushd ~/src/ff_sexy >/dev/null
    ./xpatch.sh
    su -c 'cp --no-preserve=all ./xul.so /usr/lib/firefox/libxul.so && chmod 755 /usr/lib/firefox/libxul.so'
  popd >/dev/null

  nohup firefox 2>/dev/null &
  disown
}
