function _rzt-place() {
  curl -qks 'https://ringzer0team.com/profile/1508/' | grep -i ranking | grep -oP '<span class="orange">\K[0-9]*'
}

function rzt-place() {
  echo "I'm now #$(_rzt-place) at RZT rating"
}

function rzt-status() {
  buffer=$(curl -qks 'https://ringzer0team.com/home' | grep -A3 'flag_wrapper' | grep -B5 -A3 'lolwut')
  next_name=$(echo "$buffer" | head -1 | grep -oP '<a href="[^"]*">\K[^<]*')
  next_rate=$(echo "$buffer" | grep points | head -1 | tr -dc '0-9')
  mine_rate=$(echo "$buffer" | grep points | tail -1 | tr -dc '0-9')

  echo "Next one to beat is ${next_name}"
  echo "You need only "$(( $next_rate - $mine_rate + 1))" points"
  echo "And you will be #"$(( `_rzt-place` - 1 ))" at RZT rating"
}
