function gf() {
  grep -rnw -e "function $1"
}

function ef() {
  [[ $# -gt 1 ]] && out=$(gf "$1" | sed -n "${2}p") || out=$(gf "$1")
  [[ $(echo "$out" | wc -l) -ne 1 ]] && echo "$out" && return

  f=$(echo "$out" | cut -f1 -d:)
  l=$(echo "$out" | cut -f2 -d:)
  vim "$f" "+$l"
}

function grep-mails() {
  grep -E -o "\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9.-]+\b" "$@"
}
