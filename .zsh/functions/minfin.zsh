function currency() {
  IFS=$'\n' curr=( $(wget -qO- 'http://minfin.com.ua/currency/mb/' | grep -oP '<td  class="active">\K.*') )
  echo "Buy: ${curr[1]}\nSell: ${curr[2]}" | column -s: -t
}
