function php-rebuild-evalhook() {
  pushd "${HOME}/src/php-evalhook/" >/dev/null
  make clean
  phpize && ./configure && make
  popd >/dev/null
}

function php55-rebuild-evalhook() {
  pushd "${HOME}/src/php-evalhook/" >/dev/null
  make clean
  phpize55 && ./configure && make
  popd >/dev/null
}

function php-evalhook() {
  php -d extension="${HOME}/src/php-evalhook/modules/evalhook.so" $@
}

function php55-evalhook() {
  php55 -d extension="${HOME}/src/php-evalhook/modules/evalhook.so" $@
}
