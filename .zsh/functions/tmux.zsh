function study-labs() {
  tmux new-window -a -d -n study-labs
  tmux split-window -t study-labs -h -d

  tmux set-window-option -t study-labs synchronize-panes on
  tmux send-keys -t study-labs "ssh security-lab" C-m
  tmux set-window-option -t study-labs synchronize-panes off

  tmux send-keys -t study-labs "(lxc-ls --running | grep -q 'study-one') || lxc-start -d -n study-one" C-m
  tmux send-keys -t study-labs "(lxc-ls --running | grep -q 'study-two') || lxc-start -d -n study-two" C-m

  tmux send-keys -t study-labs "lxc-attach -n study-one" C-m
  tmux select-pane -t study-labs -L
  tmux send-keys -t study-labs "lxc-attach -n study-two" C-m
  tmux select-pane -t study-labs -R

  tmux set-window-option -t study-labs synchronize-panes on
  tmux send-keys -t study-labs C-l
  tmux send-keys -t study-labs "./ctf status" C-m
  tmux set-window-option -t study-labs synchronize-panes off

  ssh security-lab "lxc-ls -f --running"
}
