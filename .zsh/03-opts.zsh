setopt AUTO_CD
setopt MULTIOS
setopt NOCORRECT
setopt AUTO_NAME_DIRS
setopt NO_BEEP
setopt inc_append_history
setopt extendedglob
setopt promptsubst
setopt complete_aliases
setopt -o emacs
setopt interactivecomments

bindkey ";5C"   forward-word          # terminator
bindkey ";5D"   backward-word         # terminator
bindkey "\e[3~" delete-char           # zsh
bindkey "^H"    backward-delete-word  # zsh
bindkey "^X^E" edit-command-line
