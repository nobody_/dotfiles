export fpath=("$HOME/.zsh/completion/" $fpath)
typeset -U fpath

export LANG="en_US.UTF-8"
export LC_PAPER="ru_RU.UTF-8"
export LC_MEASUREMENT="ru_RU.UTF-8"
export LC_TIME="ru_RU.UTF-8"
export LC_ALL="en_US.UTF-8"

export HISTFILE="$HOME/.zsh/hist/history"
export HISTSIZE=10000
export SAVEHIST=500000
export EDITOR="/usr/bin/vim"
export PAGER="/usr/bin/less"
export GOPATH="$HOME/.go"
export PATH="/usr/local/bin:/usr/bin:/usr/bin/core_perl:$HOME/.local/bin:$HOME/.bin:$GOPATH/bin"
export LESS="RFX"

## QEMU
export QEMU_LD_PREFIX=/usr/arm-linux-gnueabihf

## better don't do this, but whocares
if [ $EUID -eq 0 ]; then
  export XAUTHORITY=`eval echo "~$(id -n -u 1000)/.Xauthority"`
  export DISPLAY=":0.0"
fi
