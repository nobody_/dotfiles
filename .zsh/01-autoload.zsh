autoload -U colors compinit bashcompinit
colors
compinit
bashcompinit

autoload -z edit-command-line
zle -N edit-command-line

function hg_branch_info() {
  local current_branch=$(hg branch 2>/dev/null)
  if [[ -n $current_branch ]]; then
    echo -n "hg:%{$fg[green]%}${current_branch}%{$reset_color%}"
  fi
}

function git_branch_info() {
  local current_branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
  if [[ -n $current_branch ]]; then
    echo -n "git:%{$fg[green]%}${current_branch}%{$reset_color%}"
  fi
}

function precmd() {
  local helper="$PS1_SUFFIX"
#  curl -qks 'http://fucking-great-advice.ru/api/random_by_tag/кодеру' | jq -r '.text' | w3m -dump -T text/html
  [[ $EUID -eq 0 ]] && local suffix="!"
  export PS1="$(hg_branch_info)$(git_branch_info)${helper}%{$fg[cyan]%}${suffix}»%{$reset_color%} "
}

umask 077
