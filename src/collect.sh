#!/bin/bash
#pacman -Q | grep -E '^(ttf|xorg-fonts)' | cut -f1 -d\
DOTFILES=$(readlink -f "$0")
DOTFILES=$(dirname "${DOTFILES}")
DOTFILES=$(dirname "${DOTFILES}")
GPG_KEY="D7DB04A0018B4CAD0A06E44C006D0BF990D565F5"

function encrypt() {
  gpg --batch -ea -r "$GPG_KEY" "$1"
}

pushd "$HOME" >/dev/null
while read p; do
  absp=$(eval readlink -f "${p}")
  if [ ! -e "$absp" ]; then
    echo "${p} ???"
  fi
  rm -rf "${DOTFILES}/${p}"
  rsync -Raz "$p" "${DOTFILES}" 2>/dev/null
done < "${DOTFILES}/src/sources"
popd >/dev/null

while read p; do
  absp=$(eval readlink -f "${DOTFILES}/${p}")
  find "$absp" -type f 2>/dev/null | while read f; do
    rm -f "${f}.asc"
    encrypt "$f"
    rm -f "$f"
  done
done < "${DOTFILES}/src/encrypt"
